#!/usr/bin/env php
<?php
require_once __DIR__ . '/vendor/autoload.php';
use Kunnu\Dropbox\Dropbox;
use Kunnu\Dropbox\DropboxApp;
use Kunnu\Dropbox\DropboxFile;

// Initialize .env loading
$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

// Load variables from the .env file
$username = getenv('USERNAME', null);
$password = getenv('PASSWORD', null);
$dbName = getenv('DB_NAME', null);
$dropboxClientId = getenv('DROPBOX_CLIENT_ID', null);
$dropboxClientSecret = getenv('DROPBOX_CLIENT_SECRET', null);
$dropboxAccessToken = getenv('DROPBOX_ACCESS_TOKEN', null);
$dropboxFolderName = getenv('DROPBOX_FOLDER_NAME', null);

if(
	!$username ||
	!$password ||
	!$dbName ||
	!$dropboxClientId ||
	!$dropboxClientSecret ||
	!$dropboxAccessToken ||
	!$dropboxFolderName
) {
	echo "You are missing some details in your .env file, please check below\n";
	print_r([
		'USERNAME' => $username,
		'PASSWORD' => $password,
		'DB_NAME' => $dbName,
		'DROPBOX_CLIENT_ID' => $dropboxClientId,
		'DROPBOX_CLIENT_SECRET' => $dropboxClientSecret,
		'DROPBOX_ACCESS_TOKEN' => $dropboxAccessToken,
		'DROPBOX_FOLDER_NAME' => $dropboxFolderName,
	]);
	die();
}

// Setup backup location and name
$backupFolder = __DIR__."/backups";
$backupFileName = $dbName.'_'.date('d-m-Y_H-i-s_U').".sql";

// Test if gzip is available and use the right command
exec('gzip -V', $output, $result);

if(count($output) > 0) {
	$backupFileName = $backupFileName.'.gz';
	$command = "mysqldump --triggers --routines --single-transaction --user=$username --password=$password $dbName | gzip > $backupFolder/$backupFileName";
} else {
	$command = "mysqldump --triggers --routines --single-transaction --user=$username --password=$password $dbName > $backupFolder/$backupFileName";
}

exec($command);

// Push the backups to remote location
$app = new DropboxApp($dropboxClientId, $dropboxClientSecret, $dropboxAccessToken);
$dropbox = new Dropbox($app);
$dropboxFile = new DropboxFile("$backupFolder/$backupFileName");
$dropbox->upload($dropboxFile, "/$dropboxFolderName/$backupFileName", ['autorename' => true]);